const express = require('express');
const router = express.Router();

const sql = require('mssql');
const config = {
    user: 'CloudSAdc1c5466', // better stored in an app setting such as process.env.DB_USER
    password: '9kkQ79pR8cahx@s', // better stored in an app setting such as process.env.DB_PASSWORD
    server: 'samcord.database.windows.net', // better stored in an app setting such as process.env.DB_SERVER
    port: 1433, // optional, defaults to 1433, better stored in an app setting such as process.env.DB_PORT
    database: 'samrecord', // better stored in an app setting such as process.env.DB_NAME
    authentication: {
        type: 'default'
    },
    options: {
        encrypt: true
    },
    pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 30000
    }
}
var pool = new sql.ConnectionPool(config);

async function getRecordsQuery() {
    const connection = await pool.connect();
    var resultSet = await connection.query(`SELECT * FROM [dbo].[milkRecord] WHERE [isDeleted]=0 ORDER BY [recordTime] DESC;`);
    connection.release();
    return resultSet.recordset;
}

async function getRecordDailyQuery() {
    const connection = await pool.connect();
    var resultSet = await connection.query(`
        SELECT *, COALESCE(breastMilkAmount+formulaMilkAmount, breastMilkAmount, formulaMilkAmount, 0) AS total
        FROM(
            SELECT COUNT(id) AS numberFeedings, recordDate, SUM([breastMilkAmount]) AS breastMilkAmount, SUM([formulaMilkAmount]) AS formulaMilkAmount
            FROM(
                SELECT id, TRY_CONVERT(DATE,dateadd(HOUR, 8, [recordTime])) AS recordDate, [breastMilkAmount], [formulaMilkAmount]
                FROM [dbo].[milkRecord]
                WHERE [isDeleted]=0
            ) AS TABLE_A
            GROUP BY recordDate
        ) AS TABLE_B
        ORDER BY recordDate DESC`
    );
    connection.release();
    return resultSet.recordset;
}

async function pushRecordsQuery(body) {
    const connection = await pool.connect();
    var resultSet = await connection.query(`
        INSERT INTO [dbo].[milkRecord] ([recordTime], [breastMilkAmount], [formulaMilkAmount], [breastDuration], [hasPee], [hasPoop], [solidFood])
        OUTPUT inserted.id
        VALUES ('${body.recordTime}',${body.breastMilkAmount},${body.formulaMilkAmount},${body.breastDuration},'${body.hasPee}','${body.hasPoop}','${body.solidFood}');`);
    connection.release();
    return resultSet;
}

async function putRecordsQuery(body) {
    const connection = await pool.connect();
    var resultSet = await connection.query(`UPDATE [dbo].[milkRecord]
        SET [recordTime]='${body.recordTime}', [breastMilkAmount]=${body.breastMilkAmount}, [formulaMilkAmount]=${body.formulaMilkAmount},
        [breastDuration]=${body.breastDuration}, [hasPee]='${body.hasPee}', [hasPoop]='${body.hasPoop}', [isDeleted]='${body.isDeleted}', [solidFood]='${body.solidFood}'
        WHERE [id] = ${body.id};`);
    connection.release();
    return resultSet;
}

async function deleteRecordsQuery(id) {
    const connection = await pool.connect();
    var resultSet = await connection.query(`DELETE FROM [dbo].[milkRecord] WHERE id=${id};`);
    connection.release();
    return resultSet;
}


router.get('/records', async (req, res) => {
    var result = await getRecordsQuery();
    res.end(JSON.stringify(result));
});
router.post('/record', async (req, res) => {
    if (req.body) {
        var result = await pushRecordsQuery(req.body);
        res.end(JSON.stringify(result));
    }
});
router.put('/record', async (req, res) => {
    if (req.body) {
        var result = await putRecordsQuery(req.body);
        res.end(JSON.stringify(result));
    }
});
router.delete('/record', async (req, res) => {
    if (req.query.id != null) {
        var result = await deleteRecordsQuery(req.query.id);
        res.end(JSON.stringify(result));
    }
});

router.get('/report/daily', async (req, res) => {
    var result = await getRecordDailyQuery();
    res.end(JSON.stringify(result));
});

module.exports = router;