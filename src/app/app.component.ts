import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { DatePipe } from '@angular/common';

import { RecordDetailDialogComponent } from './record-detail-dialog/record-detail-dialog.component';
import { DailyReportDialogComponent } from './daily-report-dialog/daily-report-dialog.component';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AppComponent implements OnInit {
  title = 'samrecord';

  displayedColumns: string[] = ['recordTime', 'breastMilkAmount', 'formulaMilkAmount', 'hasPee', 'hasPoop'];
  dataSource;
  isLoading = false;
  selectedRecordId;

  constructor(
    private httpClient: HttpClient,
    public dialog: MatDialog,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    this.dataSource = [];
    this.isLoading = true;
    console.log('-----get req');
    this.getRecords().pipe(finalize(() => { this.isLoading = false; })).subscribe(
      (data) => {
        this.dataSource = this.groupUpData(data);
        console.log('----get res', this.dataSource);
      },
      (error) => {
        console.log('----get res fail');
      }
    );
  }

  addRecord() {
    this.dialog.open(RecordDetailDialogComponent, {
      panelClass: 'record-detail-dialog-container',
      minWidth: '100vw',
      minHeight: '-webkit-fill-available',
      width: '100vw',
      height: '-webkit-fill-available',
      data: {title: 'New Record'}
    }).afterClosed().subscribe((dialogResult) => {
      if (dialogResult) {
        console.log('----post req', dialogResult);
        this.isLoading = true;
        this.postRecord(dialogResult).pipe(finalize(() => { this.isLoading = false; })).subscribe(
          (data) => {
            console.log('----post res', data);
            this.selectedRecordId = data && Array.isArray(data.recordset) && data.recordset.length ? data.recordset[0].id : null;
            this.ngOnInit();
          },
          (error) => {
            console.log('----post res fail');
          }
        );
      }
    });
  };
  editRecord(record) {
    this.selectedRecordId = record.id;
    this.dialog.open(RecordDetailDialogComponent, {
      panelClass: 'record-detail-dialog-container',
      minWidth: '100vw',
      minHeight: '-webkit-fill-available',
      width: '100vw',
      height: '-webkit-fill-available',
      data: {title: 'Edit Record', recordObj: record, enableDelete: true}
    }).afterClosed().subscribe((dialogResult) => {
      if (dialogResult) {
        console.log('----put req', dialogResult);
        this.isLoading = true;
        this.putRecord(dialogResult).pipe(finalize(() => { this.isLoading = false; })).subscribe(
          (data) => {
            console.log('----put res', data);
            this.ngOnInit();
          },
          (error) => {
            console.log('----put res fail');
          }
        );
      }
    });
  }

  displayDailyRecord() {
    this.dialog.open(DailyReportDialogComponent, {
      panelClass: 'daily-report-dialog-container',
      minWidth: '100vw',
      minHeight: '-webkit-fill-available',
      width: '100vw',
      height: '-webkit-fill-available'
    });
  }


  getRecords(): Observable<any> {
    return this.httpClient.get<any>('/api/records');
  }
  postRecord(body): Observable<any> {
    return this.httpClient.post<any>('/api/record', body);
  }
  putRecord(body): Observable<any> {
    return this.httpClient.put<any>('/api/record', body);
  }

  groupUpData(records) {
    let headDate;
    if (Array.isArray(records)) {
      records.forEach(r => {
        if (!headDate || headDate !==  this.datePipe.transform(r.recordTime, 'shortDate')) {
          r.isHeadDate = true;
          headDate = this.datePipe.transform(r.recordTime, 'shortDate');
        }
      });
      return records;
    }
    return [];
  }

  splitSolidFoods(foods) {
    return foods.split(',');
  }

}
