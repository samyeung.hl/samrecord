import { Component, OnInit, Inject, ElementRef, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatBottomSheet, MatAutocompleteSelectedEvent, MatAutocomplete, MatChipInputEvent } from '@angular/material';
import { Observable } from 'rxjs/internal/Observable';
import { map, startWith } from 'rxjs/operators';
import { ConfirmDialogComponent } from '../share/confirm-dialog/confirm-dialog.component';
import {COMMA, ENTER} from '@angular/cdk/keycodes';

@Component({
  selector: 'app-record-detail-dialog',
  templateUrl: './record-detail-dialog.component.html',
  styleUrls: ['./record-detail-dialog.component.scss']
})
export class RecordDetailDialogComponent implements OnInit {
  inputValueObj = {
    recordTime: '',
    breastMilkAmount: null,
    formulaMilkAmount: null,
    breastDuration: null,
    hasPee: false,
    hasPoop: false,
    isDeleted: false,
    solidFood: ''
  };

  inputDate = new Date();
  inputTimeStr;

  title = '';
  enableDelete = false;

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  foodCtrl = new FormControl();
  filteredFoods: Observable<string[]>;
  foods: string[] = [];
  allSolidFoods: string[] = ['Apple', 'Potato', 'Sweet Potato', 'Banana', 'Pumpkin'];

  @ViewChild('fruitInput') fruitInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<RecordDetailDialogComponent>,
    private bottomSheet: MatBottomSheet
  ) { }

  ngOnInit() {
    this.title = this.data && this.data.title ? this.data.title : this.title;
    this.enableDelete = this.data && this.data.enableDelete ? this.data.enableDelete : this.enableDelete;
    if (this.data && this.data.recordObj) {
      Object.assign(this.inputValueObj, this.data.recordObj);
      this.inputDate = new Date(this.data.recordObj.recordTime);
    }

    this.inputTimeStr = this.inputDate.toTimeString();
    this.foods = this.inputValueObj.solidFood ? this.inputValueObj.solidFood.split(',') : [];

    this.filteredFoods = this.foodCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: string | null) => fruit ? this._filter(fruit) : this.allSolidFoods.slice()));
  }

  timeChange(event) {
    const timeArr = event.split(':');
    this.inputDate.setHours(timeArr[0]);
    this.inputDate.setMinutes(timeArr[1]);
  }

  save(isDeleted = false) {
    this.inputValueObj.recordTime = this.inputDate.getUTCFullYear() + '-' +
      ('0' + (this.inputDate.getUTCMonth() + 1)).slice(-2) + '-' +
      ('0' + this.inputDate.getUTCDate()).slice(-2) + 'T' +
      ('0' + this.inputDate.getUTCHours()).slice(-2) + ':' +
      ('0' + this.inputDate.getUTCMinutes()).slice(-2) + ':' +
      ('0' + this.inputDate.getUTCSeconds()).slice(-2) + '.' + this.inputDate.getUTCMilliseconds();
    this.inputValueObj.isDeleted = isDeleted;
    this.inputValueObj.solidFood = this.foods.join();
    this.dialogRef.close(this.inputValueObj);
  }

  openBottomSheet() {
    this.bottomSheet.open(ConfirmDialogComponent, {
      panelClass: 'confirm-dialog-container'
    }).afterDismissed().subscribe(result => {
      if (result) { this.save(result); }
    });
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allSolidFoods.filter(fruit => fruit.toLowerCase().indexOf(filterValue) === 0);
  }

  add(event: MatChipInputEvent): void {
    // Add fruit only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      // Add our fruit
      if ((value || '').trim()) {
        this.foods.push(value.trim());
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.foodCtrl.setValue(null);
    }
  }

  remove(food: string): void {
    const index = this.foods.indexOf(food);

    if (index >= 0) {
      this.foods.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.foods.push(event.option.viewValue);
    this.fruitInput.nativeElement.value = '';
    this.foodCtrl.setValue(null);
  }

}
