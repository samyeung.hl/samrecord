import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-daily-report-dialog',
  templateUrl: './daily-report-dialog.component.html',
  styleUrls: ['./daily-report-dialog.component.scss']
})
export class DailyReportDialogComponent implements OnInit {
  displayedColumns: string[] = ['recordDate', 'numberFeedings', 'breastMilkAmount', 'formulaMilkAmount', 'total'];
  dataSource;
  isLoading = false;

  constructor(
    private httpClient: HttpClient,
    public dialogRef: MatDialogRef<DailyReportDialogComponent>
  ) { }

  ngOnInit() {
    this.isLoading = true;
    this.getDailyRecord().pipe(finalize(() => { this.isLoading = false; })).subscribe(
      (data) => {
        this.dataSource = data;
      }
    );
  }

  getDailyRecord(): Observable<any> {
    return this.httpClient.get<any>('/api/report/daily');
  }

}
