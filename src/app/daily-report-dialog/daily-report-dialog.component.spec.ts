import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyReportDialogComponent } from './daily-report-dialog.component';

describe('DailyReportDialogComponent', () => {
  let component: DailyReportDialogComponent;
  let fixture: ComponentFixture<DailyReportDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyReportDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyReportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
