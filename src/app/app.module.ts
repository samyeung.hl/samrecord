import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DatePipe, CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatIconModule, MatTableModule, MatProgressSpinnerModule,
  MatDialogModule, MatToolbarModule, MatInputModule, MatDatepickerModule, MatNativeDateModule, MAT_DATE_LOCALE,
  MatMenuModule, MatRippleModule, MatBottomSheetModule, MatChipsModule, MatAutocompleteModule, MatExpansionModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { RecordDetailDialogComponent } from './record-detail-dialog/record-detail-dialog.component';
import { NgxMaterialTimepickerModule  } from 'ngx-material-timepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DailyReportDialogComponent } from './daily-report-dialog/daily-report-dialog.component';
import { ConfirmDialogComponent } from './share/confirm-dialog/confirm-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    RecordDetailDialogComponent,
    DailyReportDialogComponent,
    ConfirmDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    HttpClientModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatToolbarModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxMaterialTimepickerModule,
    FormsModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatRippleModule,
    MatBottomSheetModule,
    CommonModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatExpansionModule
  ],
  exports: [
    RecordDetailDialogComponent,
    DailyReportDialogComponent,
    ConfirmDialogComponent
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'en-hk'},
    DatePipe
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    RecordDetailDialogComponent,
    DailyReportDialogComponent,
    ConfirmDialogComponent
  ]
})
export class AppModule { }
