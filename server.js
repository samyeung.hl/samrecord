const express = require('express');
const path = require('path')

const api = require('./server/api');
const app = express();
const bp = require('body-parser');

app.use(bp.json());
app.use(bp.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'dist/samrecord')));
app.set('view engine', 'pug');
app.use('/api', api);

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/samrecord/index.html'));
});

const port = process.env.PORT || '8080';
app.listen(port, () => {
    console.log("Server is listening on port "+port);
});
